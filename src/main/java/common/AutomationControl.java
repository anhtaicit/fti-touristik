package common;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AutomationControl {
	public void loadControlInfo(String controlName) {
		try {

			File fXmlFile = new File(Constant.interfacesXMLPath+getPage()+".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();


					NodeList listElement = doc
							.getElementsByTagName("control");
					for (int i = 0; i < listElement.getLength(); i++) {
						Node controlNode = listElement.item(i);
						Element control = (Element) controlNode;
						if (control.getElementsByTagName("name").item(0)
								.getTextContent().equals(controlName)) {

							setControlType(control.getElementsByTagName("type")
									.item(0).getTextContent());
							setControlValue(control
									.getElementsByTagName("value").item(0)
									.getTextContent());

					}
				}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public WebElement findElement(String controlName) {
		// load control information
		loadControlInfo(controlName);
		WebElement element = null;
		String type = getControlType();
		String value = getControlValue();

		// find element by xpath
		if (type.equals("xpath")) {
			element = Constant.driver.findElement(By.xpath(value));
		}
		// find element by id
		if (type.equals("id")) {
			element = Constant.driver.findElement(By.id(value));
		}
		// find element by name
		if (type.equals("name")) {
			element = Constant.driver.findElement(By.name(value));
		}
		return element;
	}

	public WebElement findElement(String controlName, String... dynamicValue) {
		// load control information
		loadControlInfo(controlName);
		WebElement element = null;
		String type = getControlType();
		String value = getControlValue();
		value = String.format(value, dynamicValue);

		// find element by xpath
		if (type.equals("xpath")) {
			element = Constant.driver.findElement(By.xpath(value));
		}
		// find element by id
		if (type.equals("id")) {
			element = Constant.driver.findElement(By.id(value));
		}
		// find element by name
		if (type.equals("name")) {
			element = Constant.driver.findElement(By.name(value));
		}
		return element;
	}
	public By getBy(String controlName) {
		//load control information
		loadControlInfo(controlName);
		By by = null;
		String type = getControlType();
		String value = getControlValue();
		//get element by xpath value
		if (type.equals("xpath")) {
			by = By.xpath(value);
		}
		//get element by id value
		if (type.equals("id")) {
			by = By.id(value);
		}
		//get element by name value
		if (type.equals("name")) {
			by = By.name(value);
		}
		return by;
	}
	
	public By getBy(String controlName, String... dynamicValue) {
		//load control information
		loadControlInfo(controlName);
		By by = null;
		String type = getControlType();
		String value = getControlValue();
		value = String.format(value, dynamicValue);
		
		//get element by xpath value
		if (type.equals("xpath")) {
			by = By.xpath(value);
		}
		//get element by id value
		if (type.equals("id")) {
			by = By.id(value);
		}
		//get element by name value
		if (type.equals("name")) {
			by = By.name(value);
		}
		return by;
	}
	public void setControlType(String controlType) {
		this.controlType = controlType;
	}

	public String getControlType() {
		return controlType;
	}

	public void setControlValue(String controlValue) {
		this.controlValue = controlValue;
	}

	public String getControlValue() {
		return controlValue;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	private String controlValue;
	private String controlType;
	private String page;
}
